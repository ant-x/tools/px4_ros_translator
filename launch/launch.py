import os
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
  
def generate_launch_description():
    return LaunchDescription([
        Node(
            namespace='uav',
            package='px4_ros_translator',
            name='px4_ros_translator',
            executable='px4_ros_translator',
            parameters=[os.path.join(
                get_package_share_directory('px4_ros_translator'),'params','params.yaml')
                ],
            remappings=[
                ("imu/px4Msgs/in", "fmu/sensor_combined/out"),
                ("odometry/px4Msgs/in", "fmu/vehicle_odometry/out"),
                # ("imu/stdMsgs/out", ""),
                # ("odometry/stdMsgs/out", "")
                ], 
            output='screen',
        )
    ])