#ifndef PX4_ROS_TRANSLATOR__PX4_ROS_TRANSLATOR_HPP_
#define PX4_ROS_TRANSLATOR__PX4_ROS_TRANSLATOR_HPP_

#include <rclcpp/rclcpp.hpp>
#include <Eigen/Eigen>

// Messages sent from FCU to be converted to ROS2 standard messages
#include "px4_msgs/msg/sensor_combined.hpp"     // https://github.com/PX4/px4_msgs/blob/release/1.13/msg/SensorCombined.msg
#include "px4_msgs/msg/vehicle_odometry.hpp"    // https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleOdometry.msg
#include <sensor_msgs/msg/imu.hpp>              // https://docs.ros2.org/foxy/api/sensor_msgs/msg/Imu.html
#include <nav_msgs/msg/odometry.hpp>            // https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html

// Frame transformation library
#include "px4_ros_com/frame_transforms.h"

using namespace px4_ros_com::frame_transforms;
using namespace px4_ros_com::frame_transforms::utils::quaternion;
using namespace px4_ros_com::frame_transforms::utils::types;
using Matrix6d = Eigen::Matrix<double, 6, 6>;

class Px4RosTranslatorNode : public rclcpp::Node {
public:
    Px4RosTranslatorNode();

private:
    // Parameters
    std::string _odometry_link_frame;
    std::string _base_link_frame;

    // Subscribers and publishers
    rclcpp::Subscription<px4_msgs::msg::SensorCombined>::SharedPtr _sensor_combined_sub;
    rclcpp::Subscription<px4_msgs::msg::VehicleOdometry>::SharedPtr _vehicle_odometry_sub;
    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr _std_imu_pub;
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr _std_odometry_pub;

    // Input messages
    px4_msgs::msg::SensorCombined _sensor_combined_msg;
    px4_msgs::msg::VehicleOdometry _vehicle_odometry_msg;

    // Callbacks
    void sensorCombinedCallback(const px4_msgs::msg::SensorCombined::SharedPtr msg);
    void vehicleOdometryCallback(const px4_msgs::msg::VehicleOdometry::SharedPtr msg);

    // Auxiliary funcion
    void array_urt_to_covariance_matrix_custom(const std::array<float, 21> &covmsg, Matrix6d &covmat);
};

#endif // PX4_ROS_TRANSLATOR__PX4_ROS_TRANSLATOR_HPP_
