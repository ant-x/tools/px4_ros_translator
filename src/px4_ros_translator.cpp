#include "px4_ros_translator/px4_ros_translator.hpp"

Px4RosTranslatorNode::Px4RosTranslatorNode() : Node("px4_ros_translator") {
    
    this->declare_parameter<std::string>("odometry_link_frame", "odom");
    this->declare_parameter<std::string>("base_link_frame", "base_link");
    
    this->get_parameter<std::string>("odometry_link_frame", _odometry_link_frame);
    this->get_parameter<std::string>("base_link_frame", _base_link_frame);

    RCLCPP_INFO(this->get_logger(), "odometry_link_frame: %s", _odometry_link_frame.c_str());
    RCLCPP_INFO(this->get_logger(), "base_link_frame: %s", _base_link_frame.c_str());

    _sensor_combined_sub = 
        this->create_subscription<px4_msgs::msg::SensorCombined>("imu/px4Msgs/in", 10, 
            std::bind(&Px4RosTranslatorNode::sensorCombinedCallback, this, std::placeholders::_1));
    _vehicle_odometry_sub = 
        this->create_subscription<px4_msgs::msg::VehicleOdometry>("odometry/px4Msgs/in", 10, 
            std::bind(&Px4RosTranslatorNode::vehicleOdometryCallback, this, std::placeholders::_1));
        
    _std_imu_pub =
        this->create_publisher<sensor_msgs::msg::Imu>("imu/stdMsgs/out", 10);
    _std_odometry_pub = 
        this->create_publisher<nav_msgs::msg::Odometry>("odometry/stdMsgs/out", 10);
}

void Px4RosTranslatorNode::sensorCombinedCallback(const px4_msgs::msg::SensorCombined::SharedPtr msg) {
    _sensor_combined_msg = *msg;
    sensor_msgs::msg::Imu output_std_imu_msg;

    output_std_imu_msg.header.stamp = this->now();
    output_std_imu_msg.header.frame_id = _base_link_frame;

    // Convert Quaternion orientation from NED to ENU local frame (with implicit conversion from PX4 to ROS standard quaternion)
    Eigen::Quaterniond ned_orientation( _vehicle_odometry_msg.q[0], // w
                                        _vehicle_odometry_msg.q[1], // x
                                        _vehicle_odometry_msg.q[2], // y
                                        _vehicle_odometry_msg.q[3]);// z

    // static constexpr double RAD_TO_DEG = (180.0 / M_PI);
    // Eigen::Vector3d ned_euler = ned_orientation.toRotationMatrix().eulerAngles(2, 1, 0).reverse();
    // RCLCPP_INFO(this->get_logger(), "[NED] R: %.2f, P: %.2f, Y: %.2f", ned_euler.x() * RAD_TO_DEG, ned_euler.y() * RAD_TO_DEG, ned_euler.z() * RAD_TO_DEG);

    Eigen::Quaterniond enu_orientation = aircraft_to_baselink_orientation(ned_to_enu_orientation(ned_orientation));
    
    output_std_imu_msg.orientation.x = enu_orientation.x();
    output_std_imu_msg.orientation.y = enu_orientation.y();
    output_std_imu_msg.orientation.z = enu_orientation.z();
    output_std_imu_msg.orientation.w = enu_orientation.w();
    
    // static constexpr double RAD_TO_DEG = (180.0 / M_PI);
    // Eigen::Vector3d enu_euler = enu_orientation.toRotationMatrix().eulerAngles(2, 1, 0);
    // RCLCPP_INFO(this->get_logger(), "[ENU] R: %.2f, P: %.2f, Y: %.2f", enu_euler.x() * RAD_TO_DEG, enu_euler.y() * RAD_TO_DEG, enu_euler.z() * RAD_TO_DEG);

    // Convert gyroscope data from FRD to FLU body frame
    Eigen::Vector3d frd_angular_velocity(   _sensor_combined_msg.gyro_rad[0],
                                            _sensor_combined_msg.gyro_rad[1],
                                            _sensor_combined_msg.gyro_rad[2]);
    Eigen::Vector3d flu_angular_velocity = aircraft_to_baselink_body_frame(frd_angular_velocity);
    output_std_imu_msg.angular_velocity.x = flu_angular_velocity.x();
    output_std_imu_msg.angular_velocity.y = flu_angular_velocity.y();
    output_std_imu_msg.angular_velocity.z = flu_angular_velocity.z();

    // Convert accelerometer data from FRD to FLU body frame
    Eigen::Vector3d frd_acceleration(   _sensor_combined_msg.accelerometer_m_s2[0],
                                        _sensor_combined_msg.accelerometer_m_s2[1],
                                        _sensor_combined_msg.accelerometer_m_s2[2]);
    Eigen::Vector3d flu_acceleration = aircraft_to_baselink_body_frame(frd_acceleration);
    output_std_imu_msg.linear_acceleration.x = flu_acceleration.x();
    output_std_imu_msg.linear_acceleration.y = flu_acceleration.y();
    output_std_imu_msg.linear_acceleration.z = flu_acceleration.z();

    _std_imu_pub->publish(output_std_imu_msg);
}

void Px4RosTranslatorNode::vehicleOdometryCallback(const px4_msgs::msg::VehicleOdometry::SharedPtr msg) {
    _vehicle_odometry_msg = *msg;
    nav_msgs::msg::Odometry output_std_odom_msg;
    
    output_std_odom_msg.header.stamp = this->now();
    output_std_odom_msg.header.frame_id = _odometry_link_frame;
    output_std_odom_msg.child_frame_id = _base_link_frame;

    // Convert position
    if(_vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_NED) {
        // from NED to ENU body frame
        Eigen::Vector3d ned_position(   _vehicle_odometry_msg.x,
                                        _vehicle_odometry_msg.y,
                                        _vehicle_odometry_msg.z);
        Eigen::Vector3d enu_position = ned_to_enu_local_frame(ned_position);
        output_std_odom_msg.pose.pose.position.x = enu_position.x();
        output_std_odom_msg.pose.pose.position.y = enu_position.y();
        output_std_odom_msg.pose.pose.position.z = enu_position.z();

    } else if(_vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_FRD ||
                _vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::BODY_FRAME_FRD) {
        // from FRD to FLU body frame
        Eigen::Vector3d frd_position(   _vehicle_odometry_msg.x,
                                        _vehicle_odometry_msg.y,
                                        _vehicle_odometry_msg.z);
        Eigen::Vector3d flu_position = aircraft_to_baselink_body_frame(frd_position);
        output_std_odom_msg.pose.pose.position.x = flu_position.x();
        output_std_odom_msg.pose.pose.position.y = flu_position.y();
        output_std_odom_msg.pose.pose.position.z = flu_position.z();

    } else {
        // Not aligned with the std frames of reference -> do not rotate 
        output_std_odom_msg.pose.pose.position.x = _vehicle_odometry_msg.x;
        output_std_odom_msg.pose.pose.position.y = _vehicle_odometry_msg.y;
        output_std_odom_msg.pose.pose.position.z = _vehicle_odometry_msg.z;
    }

    // Convert Quaternion orientation from NED to ENU local frame (with implicit conversion from PX4 to ROS standard quaternion)
    Eigen::Quaterniond ned_orientation( _vehicle_odometry_msg.q[0], // w
                                        _vehicle_odometry_msg.q[1], // x
                                        _vehicle_odometry_msg.q[2], // y
                                        _vehicle_odometry_msg.q[3]);// z
    Eigen::Quaterniond enu_orientation = aircraft_to_baselink_orientation(ned_to_enu_orientation(ned_orientation));   
    output_std_odom_msg.pose.pose.orientation.x = enu_orientation.x();
    output_std_odom_msg.pose.pose.orientation.y = enu_orientation.y();
    output_std_odom_msg.pose.pose.orientation.z = enu_orientation.z();
    output_std_odom_msg.pose.pose.orientation.w = enu_orientation.w();
    
    // Build 6x6 pose covariance matrix
    Matrix6d cov_pose = Matrix6d::Zero();
    array_urt_to_covariance_matrix_custom(_vehicle_odometry_msg.pose_covariance, cov_pose);
    Eigen::Map<Matrix6d>(output_std_odom_msg.pose.covariance.data(), cov_pose.rows(), cov_pose.cols()) = cov_pose;

    // Convert linear velocity
    if(_vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_NED) {
        // from NED to ENU body frame
        Eigen::Vector3d ned_linear_velocity(    _vehicle_odometry_msg.vx,
                                                _vehicle_odometry_msg.vy,
                                                _vehicle_odometry_msg.vz);
        Eigen::Vector3d enu_linear_velocity = ned_to_enu_local_frame(ned_linear_velocity);
        output_std_odom_msg.twist.twist.linear.x = enu_linear_velocity.x();
        output_std_odom_msg.twist.twist.linear.y = enu_linear_velocity.y();
        output_std_odom_msg.twist.twist.linear.z = enu_linear_velocity.z();
        
    } else if(_vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_FRD ||
                _vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::BODY_FRAME_FRD) {
        // from FRD to FLU body frame
        Eigen::Vector3d frd_linear_velocity(    _vehicle_odometry_msg.vx,
                                                _vehicle_odometry_msg.vy,
                                                _vehicle_odometry_msg.vz);
        Eigen::Vector3d flu_linear_velocity = aircraft_to_baselink_body_frame(frd_linear_velocity);
        output_std_odom_msg.twist.twist.linear.x = flu_linear_velocity.x();
        output_std_odom_msg.twist.twist.linear.y = flu_linear_velocity.y();
        output_std_odom_msg.twist.twist.linear.z = flu_linear_velocity.z();

    } else {
        // Not aligned with the std frames of reference -> do not rotate 
        output_std_odom_msg.twist.twist.linear.x = _vehicle_odometry_msg.vx;
        output_std_odom_msg.twist.twist.linear.y = _vehicle_odometry_msg.vy;
        output_std_odom_msg.twist.twist.linear.z = _vehicle_odometry_msg.vz;
    }

    // Convert angular velocity from FRD to FLU body frame
    Eigen::Vector3d frd_angular_velocity(   _vehicle_odometry_msg.rollspeed,
                                            _vehicle_odometry_msg.pitchspeed,
                                            _vehicle_odometry_msg.yawspeed);
    Eigen::Vector3d flu_angular_velocity = aircraft_to_baselink_body_frame(frd_angular_velocity);
    output_std_odom_msg.twist.twist.angular.x = flu_angular_velocity.x();
    output_std_odom_msg.twist.twist.angular.y = flu_angular_velocity.y();
    output_std_odom_msg.twist.twist.angular.z = flu_angular_velocity.z();

    // Build 6x6 velocity covariance matrix
    Matrix6d cov_vel = Matrix6d::Zero();
    array_urt_to_covariance_matrix_custom(_vehicle_odometry_msg.velocity_covariance, cov_vel);
    Eigen::Map<Matrix6d>(output_std_odom_msg.twist.covariance.data(), cov_vel.rows(), cov_vel.cols()) = cov_vel;

    _std_odometry_pub->publish(output_std_odom_msg);
}

void Px4RosTranslatorNode::array_urt_to_covariance_matrix_custom(const std::array<float, 21> &covmsg, Matrix6d &covmat) {
	auto in = covmsg.begin();

	for (long int x = 0; x < covmat.cols(); x++) {
		for (long int y = x; y < covmat.rows(); y++) {
			covmat(x, y) = static_cast<double>(*in++);
			covmat(y, x) = covmat(x, y);
		}
	}
}

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);

  auto px4_ros_translator = std::make_shared<Px4RosTranslatorNode>();

  rclcpp::spin(px4_ros_translator);

  rclcpp::shutdown();
  return 0;
}
